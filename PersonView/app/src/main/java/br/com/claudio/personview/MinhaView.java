package br.com.claudio.personview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by Claudio on 22/07/16.
 */
public class MinhaView extends View {
    private Paint pincelVermelho;
    private Paint pincelPreto;
    private Paint pincelAzul;
    private Paint red;
    private Paint blue;
    private Path path;
    private RectF oval;

    private Paint p;
    private RectF rectF, rectF1;

    public MinhaView(Context context) {
        this(context, null);
    }

    public MinhaView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundColor(Color.parseColor("#00FFFFFF"));
        pincelVermelho = new Paint();
        pincelVermelho.setARGB(255, 255, 0, 0);

        pincelPreto = new Paint();
        pincelPreto.setARGB(255, 0, 0, 0);

        pincelAzul = new Paint();
        pincelAzul.setARGB(255, 0, 0, 255);

        path = new Path();

        red = new Paint();
        red.setColor(Color.RED);
        red.setStyle(Paint.Style.FILL);
        red.setStrokeWidth(5);

        blue = new Paint();
        blue.setColor(Color.BLUE);
        blue.setStyle(Paint.Style.FILL);
        blue.setStrokeWidth(5);

        oval = new RectF();

        p = new Paint();
        rectF = new RectF(20, 20, 900, 900);
        rectF1 = new RectF(120,120,800, 800);
        p.setColor(Color.BLACK);
        p.setStyle(Paint.Style.FILL_AND_STROKE);


        setFocusable(true);

    }


    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float x =(float) (460 + (Math.sin(Math.toRadians(0))*440));
        float y =(float) (460 - (Math.cos(Math.toRadians(0))*440));
        float x1 =(float) (460 + (Math.sin(Math.toRadians(0))*340));
        float y1 =(float) (460 - (Math.cos(Math.toRadians(0))*340));


        float x2 =(float) (460 + (Math.sin(Math.toRadians(210))*440));
        float y2 =(float) (460 - (Math.cos(Math.toRadians(210))*440));
        float x3 =(float) (460 + (Math.sin(Math.toRadians(210))*340));
        float y3 =(float) (460 - (Math.cos(Math.toRadians(210))*340));




        Log.d("y ", Float.toString(y));

        path.moveTo(x, y);
        path.arcTo(rectF, 270, 210);
        path.lineTo(x3,y3);
        path.arcTo(rectF1, 480, -210 );
        path.lineTo(x, y);

        path.close();


        canvas.drawPath(path, red);
        path.reset();


        float x4 =(float) (460 + (Math.sin(Math.toRadians(210))*440));
        float y4 =(float) (460 - (Math.cos(Math.toRadians(210))*440));


        float x5 =(float) (460 + (Math.sin(Math.toRadians(360))*340));
        float y5 =(float) (460 - (Math.cos(Math.toRadians(360))*340));

        path.moveTo(x4, y4);
        path.arcTo(rectF, 480, 150);
        path.lineTo(x5, y5);
        path.arcTo(rectF1, 630, -150);
        path.lineTo(x4, y4);
        path.close();


        canvas.drawPath(path, blue);




    }

}
